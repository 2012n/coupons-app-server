const express = require("express");
const multer = require("multer");
const fs = require("fs");
const path = require("path");

const db_handler = require("./database-handler.js");
const constants = require("./constants.js");

const storage = multer.diskStorage({
    destination: (request, file, cb) => {
        cb(null, ".");
    },
    filename: (request, file, cb) => {
        let pattern = /(business|owner)-(\d+)/;
        let data = pattern.exec(file.originalname);

        cb(null, `${constants.express.PROFILE_PICTURES_BASE_PATH}/${data[1]}/${data[2]}`);
    }
});
const upload = multer({
    storage: storage,
    fileFilter: (request, file, callback) => {
        if (!file.mimetype.startsWith("image/"))
            return callback(null, false);

        let pattern = /(business|owner)-(\d+).*/;
        let data = pattern.exec(file.originalname);

        if (!data)
            return callback(null, false);
        return callback(null, true);
    }
});

const router = express.Router({});

function errorResponse(response, error) {
    console.log(error);
    response.status(error.http_code);
    response.send(error);
}

router.post(constants.router.UPDATE_PROFILE_PICTURE_PATH, upload.single('profilePicture'), (request, response) => {
    response.send({
        RESPONSE_STATUS:
            request.file == null ?
                constants.response_code.PROFILE_IMAGE_BAD_FORMAT :
                constants.response_code.OPERATION_SUCCESS
    });
});

router.post(constants.router.BUSINESS_OWNER_REGISTRATION_PATH, (request, response) => {
    db_handler.registerBusinessOwner(request.body).then((result) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            profileId: result.insertId
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.post(constants.router.BUSINESS_REGISTRATION_PATH, upload.single('businessLogo'), (request, response) => {
    db_handler.registerBusiness(request.body).then((result) => {
        if (request.file != null) {
            fs.rename(request.file.filename, `${path.dirname(request.file.filename)}/${result.insertId}`, (err) => {
                if (err) {
                    console.log(err);
                    fs.unlink(request.path, () => {
                    });
                }
            });
        }

        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            id: result.insertId
        });
    }).catch((err) => {
        if (request.file != null)
            fs.unlink(request.path, (err) => {
            });
        errorResponse(response, err);
    });
});

router.post(constants.router.COUPON_REGISTRATION_PATH, (request, response) => {
    db_handler.registerCoupon(request.body).then((result) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            id: result.insertId
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.post(constants.router.UPDATE_BUSINESS_OWNER_PROFILE_PATH, (request, response) => {
    db_handler.updateBusinessOwnerProfileInfo(request.body).then(() => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.post(constants.router.UPDATE_BUSINESS_PATH, (request, response) => {
    db_handler.updateBusiness(request.body).then(() => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.post(constants.router.UPDATE_COUPON_PATH, (request, response) => {
    db_handler.updateCoupon(request.body).then(() => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});


router.post(constants.router.AUTHENTICATE_BUSINESS_OWNER_PATH, (request, response) => {
    db_handler.authenticateBusinessOwner(request.body).then((profileId) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: profileId !== constants.general.PROFILE_NOT_EXISTS_ID ?
                constants.response_code.OWNER_PROFILE_AUTHENTICATION_SUCCESS : constants.response_code.OWNER_PROFILE_AUTHENTICATION_BAD_CREDENTIALS,
            profileId: profileId
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_BUSINESS_OWNER_DATA_PATH, (request, response) => {
    db_handler.getBusinessOwnerDataById(request.query.id).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.GET_NEARBY_COUPONS, (request, response) => {
    db_handler.getNearbyCoupons(request.query).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_OWNER_BUSINESSES_PATH, (request, response) => {
    db_handler.getAllBusinessesOfBusinessOwnerByBusinessOwnerId(request.query.id).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_OWNER_COUPONS_PATH, (request, response) => {
    db_handler.getAllCouponsAssociatedWithOwnerId(request.query.id).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_BUSINESS_PATH, (request, response) => {
    db_handler.getBusinessById(request.query.id).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_COUPON_PATH, (request, response) => {
    db_handler.getCouponById(request.query.id).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.RETRIEVE_COUPON_BY_SEARCH_PATH, (request, response) => {
    db_handler.getCouponsByCretiria(request.query).then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errorResponse(response, err);
    });
});

router.get(constants.router.GET_ALL_BUSINESS_ID_AND_NAME, (request, response) => {
    db_handler.getBusinessesIdAndNameList().then((data) => {
        response.status(constants.http_status_code.COMPLETED_OK);
        response.send({
            RESPONSE_STATUS: constants.response_code.OPERATION_SUCCESS,
            data: data
        });
    }).catch((err) => {
        errResponse(response, err);
    });
});

module.exports = router;
